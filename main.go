package main

import (
	"crypto/tls"
	"log"
	"net"
	"os"
	"os/signal"
	"sync"
	"syscall"
	"time"

	"github.com/joeshaw/envdecode"
	"github.com/nsqio/go-nsq"
	"gopkg.in/mgo.v2"
)

var db *mgo.Session

type poll struct {
	Options []string
}

type Config struct {
	MongoURL string `env:"MONGO_URL,default=localhost"`
	NsqURL   string `env:"NSQ_URL,default=localhost:4150"`
}

func main() {
	var cfg Config
	if err := envdecode.Decode(&cfg); err != nil {
		log.Fatalln(err)
	}

	var stoplock sync.Mutex
	stop := false
	stopChan := make(chan struct{}, 1)
	signalChan := make(chan os.Signal, 1)
	go func() {
		<-signalChan
		stoplock.Lock()
		stop = true
		stoplock.Unlock()
		log.Println("Stopping...")
		stopChan <- struct{}{}
		closeConn()
	}()
	signal.Notify(signalChan, syscall.SIGINT, syscall.SIGTERM)

	if err := dialdb(cfg.MongoURL); err != nil {
		log.Fatalln("failed to dial MongoDb:", err)
	}
	defer closedb()

	votes := make(chan string)
	publisherStoppedChan := publishVotes(cfg.NsqURL, votes)
	twitterStoppedChan := startTwitterStream(stopChan, votes)
	go func() {
		for {
			time.Sleep(1 * time.Minute)
			closeConn()
			stoplock.Lock()
			if stop {
				stoplock.Unlock()
				return
			}
			stoplock.Unlock()
		}
	}()
	<-twitterStoppedChan
	close(votes)
	<-publisherStoppedChan
}

func publishVotes(nsqURL string, votes <-chan string) <-chan struct{} {
	stopChan := make(chan struct{}, 1)
	pub, err := nsq.NewProducer(nsqURL, nsq.NewConfig())
	if err != nil {
		log.Panic(err)
	}
	go func() {
		for vote := range votes {
			pub.Publish("votes", []byte(vote))
		}
		log.Println("Publisher: Stopping")
		pub.Stop()
		log.Println("Publisher: Stopped")
		stopChan <- struct{}{}
	}()
	return stopChan
}

func loadOptions() ([]string, error) {
	var options []string
	iter := db.DB("ballots").C("polls").Find(nil).Iter()
	var p poll
	for iter.Next(&p) {
		options = append(options, p.Options...)
	}
	iter.Close()
	return options, iter.Err()
}

func dialdb(mongoURL string) error {
	var err error
	log.Println("dialing mongodb...", mongoURL)

	tlsConfig := &tls.Config{}
	tlsConfig.InsecureSkipVerify = true
	dialInfo, err := mgo.ParseURL(mongoURL)
	dialInfo.DialServer = func(addr *mgo.ServerAddr) (net.Conn, error) {
		conn, err := tls.Dial("tcp", addr.String(), tlsConfig)
		return conn, err
	}
	db, err = mgo.DialWithInfo(dialInfo)
	return err
}

func closedb() {
	db.Close()
	log.Println("closed database connection")
}
